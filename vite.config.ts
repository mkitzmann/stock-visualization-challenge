import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// @ts-ignore
import analyze from 'rollup-plugin-analyzer'

export default defineConfig({
  plugins: [vue()],
  build: {
    rollupOptions: {
      plugins: [analyze({summaryOnly: true})],
      output: {
        manualChunks: {
          plotly: [
            'node_modules/plotly.js-basic-dist/plotly-basic.js',
          ],
        },
      }
    },
  },
})
