# Stock Visualization Challenge

A stock visualization challenge using Typescript, VueJs 3, Vite, TailwindCSS, Plotly.js and NaiveUI.

## Challenge
> Utilize Javascript or Typescript (and then utilize D3.js, Plotly.js or similar to create a chart and/or a table that visualizes stock information) to create an interactive UI that a user would love ♥️

## Demo

https://stock-visualization.netlify.app/

## Data

To improve performance I have converted the csv files to json, so the client does not have to parse the csv data for every request.

The given data had 2 issues:
1. Large file size: The original `stock-data.csv` was more than 80MB big. Loading this file into the browser and parsing it would result in a massive decrease in performance. In a real-world scenario, this large kind of data would be delivered by an API and only a subset of data displayed client side. I have therefore reduced the dataset to around 25k entries to be able to show client side handling of the data.
2. Mixed up properties: The properties `high_price`, `low_price` and `close_price` where apparently mixed up. I have adjusted the data source to the correct order (OHLC).

## Setup

### Install Dependencies

>`npm i`

### Run Dev Server

> `npm run dev`

### Build package

> `npm run build`

### Run Build Preview Server

> `npm run serve`

