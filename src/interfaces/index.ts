import {Colors} from "../enums";
import {PlotType} from "plotly.js-basic-dist";

export interface StockDataItem {
    name: string;
    date: string;
    open_price: number;
    high_price: number;
    low_price: number;
    close_price: number;
    volume: number;
    market: string;
}

export interface StockExchangeItem {
    date: string,
    total_rev: number,
    total_vol: number
}

export interface TableColumn {
    title: string,
    key: string,
    formatter?: Function,
    align?: 'left'|'right',
}

export interface SortingOptions {
    sortkey: string|number|null,
    sortOrder: SortOrder
}

export type SortOrder = 'ascend'|'descend'|false

export interface ChartOptions {
    title: string,
    color: Colors,
    type: PlotType
}
