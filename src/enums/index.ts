export enum Charts {
    TOTAL_REVENUE = 'total_rev',
    TOTAL_VOLUME = 'total_vol'
}

export enum Colors {
    GREEN = '#10B981',
    BLUE = '#3B82F6',
}
