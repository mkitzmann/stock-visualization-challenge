export const useDownloadFile = () => {

    /**
     * Provides functionality to download a file by creating a link in the DOM
     * @param url
     * @param data
     */
    const downloadFile = (
        url: string,
        data: string
    ) => {
        const anchor = document.createElement('a');
        anchor.href = data;
        anchor.target = '_blank';
        anchor.download = url;
        anchor.click();
    }

    return {
        downloadFile
    }
}
