import {ref} from "vue";

/**
 * Provides fetching data functionality, including loading state
 */
export const useLoadData = () => {
    const loading = ref(true)

    const loadData = async <T = unknown>(url: string): Promise<T> => {
        try {
            loading.value = true
            const response = await fetch(url)
            const json = await response.json()
            loading.value = false
            return json
        } catch (e) {
            throw Error('There has been an error fetching the stock data')
        }
    }

    return {
        loading,
        loadData
    }
}
