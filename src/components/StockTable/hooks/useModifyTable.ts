import {computed, onMounted, Ref, ref} from "vue";
import {SortingOptions, StockDataItem, TableColumn} from "../../../interfaces";
import {useLoadData} from "../../../hooks/useLoadData";

/**
 * Provides the ability to modify Table Data (sort, filter, offset) and all needed reactive properties
 *
 * @param tableData
 */
export const useModifyTable = (tableData: Ref<StockDataItem[]>) => {
    const selectedTicker = ref<string[]>([])
    const searchTerm = ref<string>('')
    const page = ref<number>(1)
    const pageSize = ref<number>(16)
    const sorting = ref<SortingOptions>({
        sortkey: null,
        sortOrder: false
    })

    const filteredData = computed<StockDataItem[]>(() => {
        if (tableData.value.length === 0) {
            return []
        }

        return tableData?.value.filter((row: StockDataItem) => {
                return isSelectedTicker(row)
                    && isInSearchTerm(row)
            }
        )
    })

    const isSelectedTicker = (row: StockDataItem): boolean => {
        if (selectedTicker.value.length === 0) {
            return true
        }

        return selectedTicker.value.some(ticker => ticker === row.name)
    }

    const isInSearchTerm = (row: StockDataItem): boolean => {
        if (searchTerm.value === '') {
            return true
        }

        return Object.values(row).find(
            (value: string) => {
                const base: string = value.toLowerCase()
                const compare: string = searchTerm.value.toLowerCase()

                return base.indexOf(compare) >= 0
            }
        )
    }

    const sortedData = computed<StockDataItem[]>(() => {
        const sortKey: string | number | null = sorting.value?.sortkey

        if (!sortKey || sorting.value.sortOrder === false) {
            return filteredData.value
        }

        return [...filteredData.value].sort((firstItem, secondItem) => {
            const a = String(firstItem[sortKey as keyof StockDataItem])
            const b = String(secondItem[sortKey as keyof StockDataItem])

            if (sorting.value.sortOrder === 'descend') {
                return b.localeCompare(a)
            } else {
                return a.localeCompare(b)
            }
        })
    })

    const offsetData = computed<StockDataItem[]>(() => {
        const start = (page.value - 1) * pageSize.value
        const end = page.value * pageSize.value
        return sortedData.value.slice(start, end)
    })

    return {
        selectedTicker,
        searchTerm,
        page,
        pageSize,
        sorting,
        filteredData,
        sortedData,
        offsetData,
    }
}
