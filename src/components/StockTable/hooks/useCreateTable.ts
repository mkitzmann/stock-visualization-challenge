import {h, onMounted, ref} from "vue";
import {StockDataItem, TableColumn} from "../../../interfaces";
import {useLoadData} from "../../../hooks/useLoadData";

/**
 * Loads the datasource when the parent component is mounted and includes the table configuration
 */
export const useCreateTable = () => {
    const {loading, loadData} = useLoadData()

    const moneyFormatter= (value: string) => value + ' $'

    const tableData = ref<StockDataItem[]>([])
    const columns = ref<TableColumn[]>([
            {
                title: 'Name',
                key: 'name',
                formatter: (value: string) => `<b>${value}</b>`
            },
            {
                title: 'Date',
                key: 'date',
                formatter: (date: string) => (new Date(date)).toLocaleDateString(),
            },
            {
                title: 'Open Price',
                key: 'open_price',
                align: 'right',
                formatter: (value: string) => moneyFormatter(value)
            },
            {
                title: 'High Price',
                key: 'high_price',
                align: 'right',
                formatter: (value: string) => moneyFormatter(value)
            },
            {
                title: 'Low Price',
                key: 'low_price',
                align: 'right',
                formatter: (value: string) => moneyFormatter(value)
            },
            {
                title: 'Close Price',
                key: 'close_price',
                align: 'right',
                formatter: (value: string) => moneyFormatter(value)
            },
            {
                title: 'Volume',
                key: 'volume',
                align: 'right',
                formatter: (value: string) => Number(value).toLocaleString()
            },
            {
                title: 'Market',
                key: 'market',
                align: 'right',
            },
        ]
    )

    const loadTableData = async () => {
        tableData.value = await loadData<StockDataItem[]>('../api/stock_data.json')
    }

    onMounted(async () => {
        await loadTableData()
    })

    return {
        loading,
        tableData,
        loadTableData,
        columns,
    }
}
