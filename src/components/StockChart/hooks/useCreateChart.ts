import {ChartOptions, StockExchangeItem} from "../../../interfaces";
import {ref} from "vue";
import Plotly from "plotly.js-basic-dist";
import {Charts} from "../../../enums";

export const useCreateChart = () => {
    const chartElement = ref()
    const config: Partial<Plotly.Config> = {
        responsive: true,
        displaylogo: false,
        scrollZoom: true,
        modeBarButtonsToRemove: [
            "toImage",
            "lasso2d",
            "zoom2d",
            "resetScale2d"
        ]
    }
    const layout: Partial<Plotly.Layout> = {
        showlegend: false,
        margin: {
            b: 50,
            t: 40,
            l: 60,
            r: 30,
            pad: 20
        },
        xaxis: {
            showline: false,
            showgrid: true,
        },
        yaxis: {
            showgrid: true,
            zeroline: true,
            showline: false,
        },
    }

    /**
     * Creates the Ploty chart using the config options
     *
     * @param stockData
     * @param selectedChart
     * @param options
     */
    const createChart = async (
        stockData: StockExchangeItem[],
        selectedChart: Charts,
        options: ChartOptions
    ) => {
        const data: Plotly.Data[] = [{
            type: options.type,
            x: stockData.map(item => item.date),
            y: stockData.map(item => item[selectedChart as keyof StockExchangeItem]),
            marker: {
                color: options.color
            },
            hovertemplate: options.title + ': <b>%{y:.2f}</b>' + '<br>Date: <b>%{x}</b><br>',
        }];

        chartElement.value = await Plotly.newPlot('chart', data, layout, config);
    }

    return {
        createChart,
        chartElement
    }
}
